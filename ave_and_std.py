from sys import argv
import numpy as np
import fileinput as fi

nPredictors = int(argv[1])
con = argv[3][0:-1]

list = [[] for i in range(0, nPredictors)]
count = 0
# print "start:", argv[2]
for line in fi.input(argv[2]):
    if count == 0:
        count += 1
        continue
    arr = line.split()[1:]
    [list[i].append(float(arr[i])) for i in range(0, nPredictors)]
    count += 1
# print "done"
ave = [np.mean(list[i]) for i in range(0, nPredictors)]
std = [np.std(list[i]) for i in range(0, nPredictors)]
print '{:20s}'.format('ave ' + con),
for elem in ave:
    print '{:20s}'.format(str(elem)),
print
print '{:20s}'.format('std ' + con),
for elem in std:
    print '{:20s}'.format(str(elem)),
print
