import pyshark
from sys import argv
import netaddr

target = 2154969872
for filename in argv[1:]:
    pcap = pyshark.FileCapture(filename)
    cons = {}
    packets = {}
    for pkt in pcap:
        if pkt.transport_layer is None:
            continue
        con = str(netaddr.IPAddress(pkt.ip.src)) + ' > ' + str(netaddr.IPAddress(pkt.ip.dst))
        seq = pkt.tcp.seq
        end = int(seq) + int(pkt.tcp.len)
        # print '!', seq, end
        if con not in cons:
            cons[con] = end
            packets[con] = (0.0, 1)
            continue
        if end >= cons[con]:
            cons[con] = end
        else:
            packets[con] = (packets[con][0] + 1, packets[con][1])
            # print end, cons[con]
        packets[con] = (packets[con][0], packets[con][1] + 1)
    for con in packets:
        print con + ': ' + str(packets[con][0] / packets[con][1])