for sources in 2 5 8
do
    for sinks in 2 5 8
    do
	for length in 2 6 12
	do
	    for speed in 0 1 2 3
	    do
		marker="$sources-$sinks-$length-$speed"
		count=1
		if [ $(ls connection-$marker-*.rtt | wc -l) -eq 0 ]
		then
		    for run in $(seq 1 5)
		    do
			for file in result-$marker-*-0.pcap
			do
			    python extract_rtts.py $file > temp.dat
			    python separate_rtt.py temp.dat
			    result=$(ls *-*-rtt.dat | wc -l)
			    if [ $result -gt 0 ]
			    then
				for connection in *-*-rtt.dat
				do
				    mv $connection connection-$marker-$count.rtt
				    count=$((count + 1))
				done
			    fi
			done
		    done
		fi
	    done
	done
    done
done

		    
