from machine_learning import MachineLearning
from model_based import ModelBased
from jitter import JitterPredictor
from jacobson import Jacobson
from weightedmedians import RWM

from sys import argv

predictors = [
    Jacobson(),
    RWM(),
    JitterPredictor(open(argv[1], "r")),
    ModelBased(),  
    MachineLearning(), 
]

print '{:20s}'.format('Connection'),
for p in predictors:
    print '{:20s}'.format(type(p).__name__),
print