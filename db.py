import sqlalchemy as sql
from sqlalchemy import Column, Integer, String, Unicode, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

# :memory: for testing
# echo for verbose mode
# echo for verbose mode
engine = sql.create_engine('sqlite:///example.db', echo=False)

Base = declarative_base()

class Market(Base):
    __tablename__ = 'markets'

    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __repr__(self):
        return "<Market(name='%s')>" % self.name

class Country(Base):
    __tablename__ = 'countries'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    market_id = Column(Integer, ForeignKey('markets.id'))

    market = relationship("Market", back_populates="countries")

    def __repr__(self):
        return "<Address(name='%s', market='%s')>" % (self.name, self.market)

Market.countries = relationship("Country", order_by=Country.id,
                                back_populates="market")

class City(Base):
    __tablename__ = 'cities'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    contry_id = Column(Integer, ForeignKey('countries.id'))

    country = relationship("Country", order_by=Country.id,
                           back_populates="cities")

Country.cities = relationship("City", order_by=City.id,
                              back_populates="country")

class Note(Base):
    __tablename__ = 'notes'

    id = Column(Integer, primary_key=True)
    year = Column(Integer)
    month = Column(Integer)
    day = Column(Integer)
    city_id = Column(Integer, ForeignKey('cities.id'))
    note = Column(Unicode)

    city = relationship("City", back_populates='notes')

City.notes = relationship("Note", order_by=Note.id,
                          back_populates="city")

Session = sessionmaker(bind=engine)
