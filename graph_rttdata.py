import matplotlib.pyplot as pyp
import numpy as np
from sys import argv

data = np.loadtxt(argv[1])[int(argv[2]):int(argv[3]), :]
lines = pyp.plot(data)
pyp.legend(lines, ['RTT', 'Machine Learning', 'Model Based', 'Jitter Predictor', 'RWM', 'Jacobson'], loc = 'best')
pyp.show()