#Alice Lim
#Eifel 
#alternitive timers fo the transmission protocall (TCP)

from rttpredictor import  RTTPredictor

#"the following set of equations define RTOL (L = lite)"

class Eifel(RTTPredictor):
	def __init__ (self, under_error=1, over_error=-1):
		RTTPredictor.__init__(self, under_error, over_error)
		self.gain = 1.0/3.0
		self.srtt = None
		self.rttvar = None


	def update_rto (self, rtt, time):
		if self.srtt is None:
			self.srtt = rtt
			self.rttvar = rtt/2
			self.rto = max((self.srtt + 1.0/self.gain*self.rttvar), rtt + 2*self.granularity)
			
		else:
			#delta starts calculating after the third iteration 
			delta = rtt - self.srtt 
			self.srtt = self.srtt + self.gain*delta

			#calculate the gain bar based on certain conditions
			if delta - self.rttvar >= 0:
				gainbar = self.gain
			else: 
				gainbar = self.gain**2.0

			#calculate rttvar based on certain conditions
			if delta < 0:
				self.rttvar = self.rttvar
			else: 
				self.rttvar = self.rttvar + gainbar*(delta - self.rttvar)

			#recalcuate rto because now you have delta 
			self.rto = max((self.srtt + 1.0/self.gain*self.rttvar), rtt + 2*self.granularity)
		return self.rto



	def process_loss(self, rtt, time):
		RTTPredictor.process_loss(self, rtt, time)
		self.rto = min(2*self.rto, 60*1000)
		return self.rto