#!/usr/bin/python

from machine_learning import MachineLearning
from model_based import ModelBased
from jitter import JitterPredictor
from jacobson import Jacobson
from weightedmedians import RWM

from sys import argv
import fileinput
import datetime

if __name__ == "__main__":
    predictors = [
        Jacobson(),
        RWM(),
        JitterPredictor(open(argv[1], "r")),
        ModelBased(),  
        MachineLearning(), 
    ]

    filename = argv[2]
    count = 0
    with  open(filename, 'r') as f:
        for line in f:
            arr = line.strip().split()
            rtt = float(arr[0])
            time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S.%f")
            for p in predictors:
                p.receive_rtt(rtt, time)
            count += 1
    print '{:20s}'.format(filename),
    for p in predictors:
        print '{:20s}'.format(str(p.lost_packets*100.0/count)),
    print

    '''count = 0
    for line in fileinput.input(argv[2:]):
        arr = line.strip().split()
        rtt = float(arr[0])
        print '{:20s}'.format(str(rtt)),
        time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S.%f")
        for p in predictors:
            p.receive_rtt(rtt, time)
            print '{:20s}'.format(str(p.rto)),
        print
        count += 1'''