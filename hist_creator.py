import datetime
import numpy as np
import matplotlib.pyplot as plt
from fileinput import input

count = 1
hist = {}
prevRTT = None
prevTime = None
for line in input():
    arr = line.strip().split()
    rtt = float(arr[0])
    time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S.%f")
    if count == 1:
        prevRTT = rtt
        prevTime = time
        count += 1
        continue
    td = (time - prevTime).microseconds
    if td not in hist:
        hist[td] = []
    hist[td].append(rtt - prevRTT)
    prevRTT = rtt
    prevTime = time
    count += 1
for key in hist:
    arr = np.asarray(hist[key])
    # TODO: Original seemed wrong
    # in the paper: c_2(\tau) = sqrt(E[(X(t+tau)-X(t))^2])
    hist[key] = np.sqrt(np.mean(arr**2))

plt.hist(hist.keys(), 100)
plt.gca().set_yscale("log")
plt.show()