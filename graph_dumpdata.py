import matplotlib.pyplot as plt
from fileinput import input
from sys import argv
import numpy as np

'''nPredictors = int(argv[1]) + 1'''
nPredictors = int(argv[1])

list = []
labels = []
count = 0
legend = []
name = None
for line in input(argv[2:]):
    # print line
    arr = line.strip().split()
    # print arr[0]
    # print line.strip().split()[0:3]
    if count == 0:
        legend = arr[1:]
    else:
        name = arr[0].split('-')[0:-1]
        list.append([np.log(float(elem) * 100) for elem in arr[1:]])
        if len(arr[0]) < 11:
            labels.append(arr[0])
        else:
            labels.append(arr[0][11:-4])
    count += 1

# print labels

colors = ['blue', 'green', 'red', 'cyan', 'magenta']
'''for i in range(0, nPredictors):
    colors.append(np.random.rand(3,))'''

ax = plt.subplot(111)
w = 1 / 3.0
x = [num for num in np.arange(0, len(list[0]) * w, w)]
i = 0
labelpos = []
for arr in list:
    # print arr
    for count in range(0, len(x)):
        if len(labelpos) == 0:
            ax.bar(x[count] + i, arr[count], width=w, color = colors[count], align='center', linewidth = 0, label = legend[count])
        else:
            ax.bar(x[count] + i, arr[count], width=w, color = colors[count], align='center', linewidth = 0)
    i += nPredictors * w + 2
    labelpos.append(x[0] + i)
# ax.autoscale(tight=True)

handles, l = ax.get_legend_handles_labels()
ax.legend(handles, legend)

plt.legend(loc = 'best')
plt.xticks(labelpos, labels, rotation = -60, ha = 'right')
plt.gcf().subplots_adjust(bottom=0.2, left = 0.2)
plt.ylabel('Loss (logscale)')
# plt.show()
plt.savefig('-'.join(name) + '.png')
print 'done'