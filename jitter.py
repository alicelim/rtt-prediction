# Arjun Subramonian
# Jitter-Based Delay-Boundary Prediction of Wide-Area Networks

from rttpredictor import RTTPredictor
import datetime
import numpy as np
from bisect import bisect_left

class JitterPredictor(RTTPredictor):
    def __init__(self, f, under_error = 1, over_error = -1, k = 2.6):
        RTTPredictor.__init__(self, under_error, over_error)
        self.count = 1
        self.hist = {}
        prevRTT = None
        self.prevTime = None
        for line in f:
            arr = line.strip().split()
            rtt = float(arr[0])
            time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S.%f")
            if self.count == 1:
                prevRTT = rtt
                self.prevTime = time
                self.count += 1
                continue
            td = (time - self.prevTime).total_seconds()
            if td not in self.hist:
                self.hist[td] = []
            self.hist[td].append(rtt - prevRTT)
            prevRTT = rtt
            self.prevTime = time
            self.count += 1
        for key in self.hist:
            arr = np.asarray(self.hist[key])
            # TODO: Original seemed wrong
            # in the paper: c_2(\tau) = sqrt(E[(X(t+tau)-X(t))^2])
            self.hist[key] = np.sqrt(np.mean(arr**2))
        self.k = k

        self.count = 1
        self.prevTime = None

    def find_ge(self, a, key):
        '''Find smallest item greater-than or equal to key.
        Raise ValueError if no such item exists.
        If multiple keys are equal, return the leftmost.

        '''
        i = bisect_left(a, key)
        if i == len(a):
            return -1
        return a[i]

    def update_rto(self, rtt, time):
        if self.count != 1:
            td = (time - self.prevTime).total_seconds()
            ge = self.find_ge(self.hist.keys(), td)
            if ge != -1:
                self.rto = rtt - self.k * self.hist[ge]
            else:
                self.rto = rtt - self.k * self.hist[self.hist.keys()[-1]]
        self.prevTime = time
        self.count += 1

    def process_loss(self, rtt, time):
        RTTPredictor.process_loss(self, rtt, time)
        if self.count != 1:
            td = (time - self.prevTime).total_seconds()
            ge = self.find_ge(self.hist.keys(), td)
            if ge != -1:
                self.rto = rtt + self.k * self.hist[ge]
            else:
                self.rto = rtt + self.k * self.hist[self.hist.keys()[-1]]
        self.prevTime = time
        self.count += 1

