all: trace_rtts.dat
	python rtt_test.py trace_rtts.dat

trace_rtts.dat: tcp_exchange.dat
	python calc_rtt.py tcp_exchange.dat > trace_rtts.dat

tcp_exchange.dat:
	python parse_pcap.py dump.pcap > tcp_exchange.dat
