from sys import argv
from parse import process_line
from fileinput import input
import netaddr
from subprocess32 import Popen, PIPE
from datetime import datetime

class RTTCalculator(object):
    def __init__(self, src, dst):
        self.src = src
        self.dst = dst
        self.times = {}

    def process(self, pkt):
        src = int(netaddr.IPAddress(pkt[1][0]))
        dst = int(netaddr.IPAddress(pkt[1][1]))
        #import pdb; pdb.set_trace()
        if 'options' not in pkt[3] or 'TS' not in pkt[3]['options']:
            return
        if dst == self.dst:
            try:
                ts = pkt[3]['options']['TS']['val']
            except KeyError:
                return
            if ts not in self.times:
                self.times[ts] = pkt[0]
        else:
            try:
                ts = pkt[3]['options']['TS']['ecr']
            except AttributeError:
                return
            return (pkt[0] - self.times[ts], self.times[ts])

def starting(pkt):
    if 'syn' in pkt[2] and 'ack' not in pkt[2]:
        return True
    return False
    
# this doesn't work in the general case, but for me sources and destinations
# will never exchange roles
def get_pair(src, dst, sources, pkt):
    if starting(pkt):
        sources.add(src)
    if src in sources:
        return "%d-%d" % (src, dst)
    if dst in sources:
        return "%d-%d" % (dst, src)

calculators = {}
sources = set()
for file in argv[1:]:
    pipe = Popen(["tcpdump", "-r", file, "-tt", "-n"], stdout=PIPE).stdout
    for line in pipe:
        pkt = process_line(line)
        if pkt is None:
            continue
        src = int(netaddr.IPAddress(pkt[1][0]))
        dst = int(netaddr.IPAddress(pkt[1][1]))
        pair = get_pair(src, dst, sources, pkt)
        if pair is not None:
            if pair not in calculators:
                if starting(pkt):
                    calculators[pair] = RTTCalculator(src, dst)
                else:
                    continue
            ret = calculators[pair].process(pkt)
            if ret is not None:
                print src, dst, ret[0] * 1000, datetime.utcfromtimestamp(ret[1]).isoformat()
