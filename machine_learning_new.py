#Alice Lim 
#Machine Learning approach to RTT prediction  

from rttpredictor import RTTPredictor
import numpy as np
import math

class Sense(RTTPredictor):
    def __init__(self, under_error=1, k=4, over_error=-1, a=1/8, b=1/4, beta = 2.0, nmin= 10, nmax = 100, el = 0.01 ):
        """Initializes the Jacobson algorithm (RFC6298)."""
        RTTPredictor.__init__(self, under_error, over_error)
        self.srtt = None
        self.rttvar = None
        self.k = k
        self.a = a
        self.b = b
        self.beta = beta
        self.nmin = nmin
        self.nmax = nmax
        self.el = el
        self.loss = np.zeros(k)
        self.experts = np.zeros(k)
        self.rtt_max = None
        self.expert_weights = np.zeros(k)
        self.alpha = np.zeros(k)
        step = 1.0/(k+1.0)
        for i in range(k): 
            self.alpha[i] = (i+1)*step  
        self.norm_error = np.zeros(k)
        self.n_array = np.zeros(k)
        self.eta_list = []
        self.rtt_list = []
        

    #set experts and alpha list
    def ewma_experts(self, rtt):

        self.experts = rtt * self.alpha + (1-self.alpha) * self.experts
        

    def loss_function(self, rtt):
        if rtt > self.rtt_max:
            self.rtt_max = rtt

        self.norm_error = abs(self.experts - rtt)/self.rtt_max

        for i in range (len(self.norm_error)):
            if abs(self.norm_error[i]) <= self.el:
                self.loss[i] = None 
            else:
                self.loss[i] = self.norm_error[i]

        for i in range (len(self.norm_error)):
            if self.norm_error[i] > self.norm_error[i-1] > self.norm_error[i-2]:
                self.n_array[i] = min(self.nmax, (self.n_array[i]*self.beta))
            elif self.norm_error[i] < self.norm_error[i-1] < self.norm_error[i-2]:
                self.n_array[i] = max(self.nmin, (self.n_array[i]/self.beta))

        self.eta_list.append(self.n_array.copy())

    def level_shift(self):
        for i in range (1, len(self.rtt_list)-2):
            a = self.rtt_list[:i]
            b = self.rtt_list[i:]
            chi = 1./3.* np.median(a)
            if (max(a) < min(b) and np.median(a) < np.median(b) - chi) or (
                min(a) > max(b) and np.median(a) > np.median(b) + chi):
                return i
        return -1

    def weight_update(self, rtt):
        # if self.rto > 2719:
        #     import pdb; pdb.set_trace()
        self.loss_function(rtt)
        index = np.logical_not(np.isnan(self.loss))
        self.expert_weights[index] = self.expert_weights[index]*np.exp(-self.n_array[index]*self.loss[index])
        k = self.level_shift()
        if k > 0:
            c = np.array(self.eta_list[k-1]+self.eta_list[k])
            self.eta_list = self.eta_list[k:]
            self.rtt_list = self.rtt_list[k:]
            self.rtt_max = max(self.rtt_list)
            self.expert_weights[index] = self.expert_weights[index]*np.exp(c[index]*self.loss[index])
        self.expert_weights *= 1.0/np.sum(self.expert_weights)


    def update_rto(self, rtt, time):
        self.rtt_list.append(rtt)
        if self.srtt is None:
            self.srtt = rtt
            self.rttvar = rtt/2
            self.rto = self.srtt + max(self.granularity,
                                       self.k * self.rttvar)
            self.experts += rtt
            self.expert_weights += 1.0/(len(self.experts))
        else:
            self.rttvar = (1 - self.b) * self.rttvar + (
                self.b * abs(self.srtt - rtt))
            self.ewma_experts(rtt)
            self.srtt = np.sum(self.expert_weights*self.experts) / np.sum(self.expert_weights)
            self.weight_update(rtt)
            self.rto = self.srtt + max(self.granularity,
                                       self.k * self.rttvar)
        # print rtt, self.rto
        return self.rto

    def process_loss(self, rtt, time):
        RTTPredictor.process_loss(self, rtt, time)
        self.rto = min(2*self.rto, 60*1000)
        return self.rto
